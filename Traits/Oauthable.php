<?php

namespace Weelis\Oauth\Traits;

trait Oauthable
{
    /**
     * Check scope using for oauth
     *
     * @return bool
     */
    abstract public function canScope($scope = null);
}