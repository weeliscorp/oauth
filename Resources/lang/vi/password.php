<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed'    => 'Sai thông tin tài khoản',
	'params'    => 'Thiếu thông tin tài khoản',
	'user_info' => 'Không thể lấy thông tin người dùng',
	'scope'     => 'Đăng nhập thất bại',
	'user_id'   => 'You must set the user_id on the array returned by getUserDetails',

];