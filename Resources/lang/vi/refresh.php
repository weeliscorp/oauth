<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed'  => 'Sai thông tin refresh token',
	'expired' => 'Refresh token đã hết hạn',
	'params'  => 'Thiếu thông tin "refresh_token"'

];