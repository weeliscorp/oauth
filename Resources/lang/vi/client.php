<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed'    => 'Chứng thực ứng dụng không hợp lệ',
	'exception' => 'Chứng thực cần "client_id"',
	'params'    => 'Chứng thực cần "client_secrect"',
];