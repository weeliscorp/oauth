<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed'    => 'Invalid username and password combination',
	'params'    => 'Missing parameters: "username" and "password" required',
	'user_info' => 'Unable to retrieve user information',
	'scope'     => 'Invalid scope',
	'user_id'   => 'You must set the user_id on the array returned by getUserDetails',
];