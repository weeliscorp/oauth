<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed'    => 'The client credentials are invalid',
	'exception' => 'the clientData array must have "client_id" set',
	'params'    => 'client credentials are required',
];