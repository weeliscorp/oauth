<?php

namespace Weelis\Oauth\Models;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'client_id', 'client_secret', 'redirect_uri', 'grant_types', 'scope', 'user_id'
	];
	public $timestamps = false;

	/**
	 * [roles description]
	 * @return [type] [description]
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
