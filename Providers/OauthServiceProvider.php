<?php namespace Weelis\Oauth\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use DB;
use OAuth2\Scope;
use OAuth2\Server as OAuth2Server;
use OAuth2\Storage\Memory;
use Redis;
use Weelis\Oauth\Http\Middleware\OauthMiddleware;
use Weelis\Oauth\Oauth2\Client\OauthJwtClient;
use Weelis\Oauth\Oauth2\GrantType\ClientCredentials;
use Weelis\Oauth\Oauth2\GrantType\JwtBearer;
use Weelis\Oauth\Oauth2\GrantType\RefreshToken;
use Weelis\Oauth\Oauth2\GrantType\UserCredentials;
use Weelis\Oauth\Oauth2\Storage\PdoStorage;
use Weelis\Oauth\Oauth2\Storage\RedisStorage;

class OauthServiceProvider extends ServiceProvider
{

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'oauth');
		$this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
		$this->publishes([
			__DIR__ . '/../Config/config.php' => config_path('oauth.php'),
		]);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/../Config/config.php', 'oauth'
		);
		$this->app->singleton('oauth2', function ($app) {
			$storage = new PdoStorage(DB::connection()->getPdo(), $app['config']['oauth.password']);
			$storage_redis = new RedisStorage(Redis::connection(), $app['config']['oauth.password']);
			$server = new OAuth2Server($storage);
			if ($app['config']['oauth.access_token_redis']) {
				$server->addStorage($storage_redis, 'access_token');
				$server->addStorage($storage_redis, 'refresh_token');
			}
			foreach ($app['config']['oauth.grant_types'] as $grant_type) {
				if ($grant_type == "client_credentials") {
					if ($app['config']['oauth.access_token_redis']) {
						$server->addGrantType(new ClientCredentials($storage_redis));
					} else {
						$server->addGrantType(new ClientCredentials($storage));
					}

				} elseif ($grant_type == "jwt_bearer") {
					if ($app['config']['oauth.access_token_redis']) {
						$server->addGrantType(new JwtBearer($storage_redis, $app['config']['oauth.jwt.audience'], $app['config']['oauth.required_scope']));
					} else {
						$server->addGrantType(new JwtBearer($storage, $app['config']['oauth.jwt.audience'], $app['config']['oauth.required_scope']));
					}
				} elseif ($grant_type == "password") {
					if ($app['config']['oauth.access_token_redis']) {
						$server->addGrantType(new UserCredentials($storage_redis, $app['config']['oauth.password'], $app['config']['oauth.required_scope']));
					} else {
						$server->addGrantType(new UserCredentials($storage, $app['config']['oauth.password'], $app['config']['oauth.required_scope']));
					}
				} elseif ($grant_type == "refresh_token") {
					if ($app['config']['oauth.access_token_redis']) {
						$server->addGrantType(new RefreshToken($storage_redis, $app['config']['oauth.refresh_token']));
					} else {
						$server->addGrantType(new RefreshToken($storage, $app['config']['oauth.refresh_token']));
					}
				}
			}

			if ($app['config']['oauth.required_scope']) {
				$memory = new Memory([
					'default_scope'    => $app['config']['oauth.scopes.default'],
					'supported_scopes' => $app['config']['oauth.scopes.supports']
				]);
				$scopeUtil = new Scope($memory);
				$server->setScopeUtil($scopeUtil);
			}

			return $server;
		});
		$this->app->singleton('oauth2.jwt_client', function ($app) {
			return new OauthJwtClient(
				$app['config']['oauth.jwt']
			);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['oauth2', 'oauth2.jwt_client'];
	}

}
