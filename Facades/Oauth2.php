<?php
namespace Weelis\Oauth\Facades;

use Illuminate\Support\Facades\Facade;


class Oauth2 extends Facade {

    protected static function getFacadeAccessor() { return 'oauth2'; }
}