<?php
namespace Weelis\Oauth\Facades;

use Illuminate\Support\Facades\Facade;


class JwtClient extends Facade {

    protected static function getFacadeAccessor() { return 'oauth2.jwt_client'; }
}