<?php

namespace Weelis\Oauth\Http\Middleware;

use Closure;
use OAuth2\HttpFoundationBridge\Request as Oauth2Request;
use OAuth2\HttpFoundationBridge\Response as Oauth2Response;
use Oauth2;

class OauthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bridgedRequest  = Oauth2Request::createFromRequest($request);
        $bridgedResponse = new Oauth2Response();

        if (Oauth2::verifyResourceRequest($bridgedRequest, $bridgedResponse)) {
            $params = Oauth2::getAccessTokenData($bridgedRequest,$bridgedResponse);
            if(isset($params['user_id'])) {
                $request->setUserResolver(function()use($params){
                    return app()->make(config('oauth.password.user_model'))->find($params["user_id"]);
                });
            }
            $request->merge([
            	'oauth' => [
		            'access_token'=> $params['access_token'],
		            'client_id' => $params["client_id"],
		            'scope' => $params["scope"]
	            ]
            ]);
            return $next($request);
        }
        $response = response('Unauthorized.', 401);
        
        return $response;
    }
}
