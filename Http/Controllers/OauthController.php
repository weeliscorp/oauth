<?php namespace Weelis\Oauth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use OAuth2\HttpFoundationBridge\Request as Oauth2Request;
use OAuth2\HttpFoundationBridge\Response as Oauth2Response;
use Oauth2;

class OauthController extends Controller {
	
	public function index(Request $request)
	{
		$bridgedRequest  = Oauth2Request::createFromRequest($request->instance());
		$bridgedResponse = new Oauth2Response();

		$bridgedResponse = Oauth2::handleTokenRequest($bridgedRequest, $bridgedResponse);

		return $bridgedResponse;
	}
}