<?php

Route::group([
	'middleware' => 'cors',
	'domain' => config('weelis.api_domain'),
	'prefix' => 'oauth',
	'namespace' => 'Weelis\Oauth\Http\Controllers'], function()
{
	// Get oauth token
	Route::post('token', 'OauthController@index');
	//Login oauth token
	Route::post('login','OauthController@postLogin');
});