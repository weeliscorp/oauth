<?php

return [
	'name'               => 'Oauth',
	'access_token_redis' => false,
	'required_scope'     => false,
	'grant_types'        => [
		"client_credentials", "password", "refresh_token" //, "implicit", "authorization_code", "jwt_bearer"
	],
	'scopes'             => [
		"default"  => "user",
		"supports" => [
			"user"
		]
	],
	'refresh_token'      => [
		'always_issue_new_refresh_token' => true,
		'refresh_token_lifetime'         => 2419200
	],
	'password'           => [
		'user_model'     => \App\User::class,
		'userid_field'   => 'id',
		'username_fields' => ['email'], // ['phone', 'email']
		'password_field' => 'password',
		'social_field'    => 'social_id',
		'active_field'   => null
	],
	'jwt'                => [
		'audience'    => env('APP_URL'),
		'public_key'  => storage_path('ssl/public.pem'),
		'private_key' => storage_path('ssl/private.pem')
	]
];