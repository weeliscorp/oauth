<?php
/**
 * Created by IntelliJ IDEA.
 * User: simonnguyen
 * Date: 3/17/16
 * Time: 11:34 AM
 */

namespace Weelis\Oauth\Helpers;

abstract class OtpType
{
    const All = 0;
    const Number = 1;
    const Upper = 2;
    const UpperNumber = 3;
    const Lower = 2;
    const LowerNumber = 3;
}

class Helpers
{
    public static function genRandomOtp($length = 6, $type = OtpType::All)
    {
        switch ($type) {
            case OtpType::Number:
                $chars = "23456789";
                break;
            case OtpType::Upper:
                $chars = "23456789ABCDEFGHKMNPQRSTUVXYZ";
                break;
            case OtpType::UpperNumber:
                $chars = "23456789ABCDEFGHKMNPQRSTUVXYZ";
                break;
            case OtpType::Upper:
                $chars = "abcdefghkmnpqrstuvxyz";
                break;
            case OtpType::UpperNumber:
                $chars = "23456789abcdefghkmnpqrstuvxyz";
                break;
            default:
                $chars = "23456789abcdefghkmnpqrstuvxyzABCDEFGHKMNPQRSTUVXYZ";
                break;
        }
        $final_rand = '';
        for ($i = 0; $i < $length; $i++) {
            $final_rand .= $chars[rand(0, strlen($chars) - 1)];

        }
        return $final_rand;
    }
}