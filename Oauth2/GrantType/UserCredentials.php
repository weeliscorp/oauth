<?php

namespace Weelis\Oauth\Oauth2\GrantType;

use OAuth2\GrantType\GrantTypeInterface;
use OAuth2\RequestInterface;
use OAuth2\ResponseInterface;
use OAuth2\ResponseType\AccessTokenInterface;
use OAuth2\Storage\UserCredentialsInterface;
use OAuth2\ClientAssertionType\ClientAssertionTypeInterface;

class UserCredentials implements GrantTypeInterface, ClientAssertionTypeInterface
{
    private $userInfo;

    protected $storage;
    protected $config;
    protected $validScope;

    /**
     * @param OAuth2\Storage\UserCredentialsInterface $storage REQUIRED Storage class for retrieving user credentials information
     */
    public function __construct(UserCredentialsInterface $storage, array $config, $validScope)
    {
        $this->storage = $storage;
        $this->config = $config;
        $this->validScope = $validScope;
    }

    public function getQuerystringIdentifier()
    {
        return $this->config['password_field'];
    }

    public function validateRequest(RequestInterface $request, ResponseInterface $response)
    {
        if (!$request->request("password") || !$request->request("username")) {
            $response->setError(400, 'invalid_request', __('oauth::password.params'));
            $response->addParameters(["status" => 0, "message" => __('oauth::password.params')]);
            return null;
        }

        if (!$this->storage->checkUserCredentials($request->request("username"), $request->request("password"))) {
            $response->setError(401, 'invalid_grant', __('oauth::password.failed'));
            $response->addParameters(["status" => 0, "message" => __('oauth::password.failed')]);

            return null;
        }

        $userInfo = $this->storage->getUserDetails($request->request("username"));

        if (empty($userInfo)) {
            $response->setError(400, 'invalid_grant', __('oauth::password.user_info'));
            $response->addParameters(["status" => 0, "message" => __('oauth::password.user_info')]);

            return null;
        }

        if($this->validScope) {
            if(is_array($request->request("scope"))) {
                foreach ($request->request("scope") as $scope) {
                    if(!$this->checkScope($scope)) {
                        $response->setError(400, 'invalid_grant', __('oauth::password.scope'));
                        $response->addParameters(["status" => 0, "message" => __('oauth::password.scope')]);
                        return null;
                    }
                }
            } else {
                if(!$this->checkScope($request->request("scope"))) {
                    $response->setError(400, 'invalid_grant', __('oauth::password.scope'));
                    $response->addParameters(["status" => 0, "message" => __('oauth::password.scope')]);
                    return null;
                }
            }

        }

        if (!isset($userInfo['user_id'])) {
            throw new \LogicException(__('oauth::password.user_id'));
        }

        $this->userInfo = $userInfo;

        return true;
    }

    protected function checkScope($scope) {
        return \Auth::user()->canScope($scope);
    }

    public function getClientId()
    {
        return $this->userInfo[$this->config['userid_field']];
    }

    public function getUserId()
    {
        return $this->userInfo['user_id'];
    }

    public function getScope()
    {
        return isset($this->userInfo['scope']) ? $this->userInfo['scope'] : null;
    }

    public function createAccessToken(AccessTokenInterface $accessToken, $client_id, $user_id, $scope)
    {
        return $accessToken->createAccessToken($client_id, $user_id, $scope);
    }
}