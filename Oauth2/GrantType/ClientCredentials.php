<?php

namespace Weelis\Oauth\Oauth2\GrantType;

use OAuth2\GrantType\GrantTypeInterface;
use OAuth2\RequestInterface;
use OAuth2\ResponseInterface;
use OAuth2\ResponseType\AccessTokenInterface;
use OAuth2\Storage\UserCredentialsInterface;
use OAuth2\ClientAssertionType\ClientAssertionTypeInterface;

class ClientCredentials extends \OAuth2\GrantType\ClientCredentials implements GrantTypeInterface
{
	private $clientData;

	/**
	 * Get scope
	 *
	 * @return string|null
	 */
	public function getScope()
	{
		$this->loadClientData();

		return isset($this->clientData['scope']) ? $this->clientData['scope'] : null;
	}

	/**
	 * Get the client id
	 *
	 * @return mixed
	 */
	public function getClientId()
	{
		return $this->clientData['client_id'];
	}

	/**
	 * Get user id
	 *
	 * @return mixed
	 */
	public function getUserId()
	{
		$this->loadClientData();

		return isset($this->clientData['user_id']) ? $this->clientData['user_id'] : null;
	}

	private function loadClientData()
	{
		if (!$this->clientData) {
			$this->clientData = $this->storage->getClientDetails($this->getClientId());
		}
	}

	/**
	 * Validate the OAuth request
	 *
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @return bool|mixed
	 * @throws LogicException
	 */
	public function validateRequest(RequestInterface $request, ResponseInterface $response)
	{
		if (!$clientData = $this->getClientCredentials($request, $response)) {
			return false;
		}

		if (!isset($clientData['client_id'])) {
			throw new LogicException(__('oauth::client.exception'));
		}

		if (!isset($clientData['client_secret']) || $clientData['client_secret'] == '') {
			if (!$this->config['allow_public_clients']) {
				$response->setError(400, 'invalid_client', __('oauth::client.params'));
				$response->addParameters(["status" => 0, "message" => __('oauth::client.params')]);
				return false;
			}

			if (!$this->storage->isPublicClient($clientData['client_id'])) {
				$response->setError(400, 'invalid_client', __('oauth::client.client_secrect'));
				$response->addParameters(["status" => 0, "message" => __('oauth::client.client_secrect')]);
				return false;
			}
		} elseif ($this->storage->checkClientCredentials($clientData['client_id'], $clientData['client_secret']) === false) {
			$response->setError(400, 'invalid_client', __('oauth::client.failed'));
			$response->addParameters(["status" => 0, "message" => __('oauth::client.failed')]);
			return false;
		}

		$this->clientData = $clientData;

		return true;
	}
}