<?php

namespace Weelis\Oauth\Oauth2\Client;

use Cache;
use cURL;
use File;
use Oauth2;
use Helpers;

/**
 * Oauth client
 * User: simonnguyen
 * Date: 08/18/16
 * Time: 11:43 AM
 */
class OauthClient
{
	private $token_cache_key;
	private $token_url;
	private $client_id;
	private $client_secret;
	private $resource_url;
	private $content_type;

	public function __construct($config = [])
	{
		$this->token_url = $config['oauth_token_url'];
		$this->client_id = $config['client_id'];
		$this->client_secret = $config['client_secret'];
		$this->token_header = $config['oauth_token_header'];

		$this->resource_url = $config['resource_url'];
		$this->content_type = $config['content_type'];
		$this->token_cache_key = $config['cache_token_key'];
	}

	public function getResource($path, $code, array $payload = [], $method = 'post')
	{
		$token = $this->getToken($code);
		if (isset($token)) {
			// Remain token
			$auths = $this->token_header . $token;
			$auth_header = explode(':', $auths);
			if(is_array($auth_header)) {
				$auth_header = array_map('trim', $auth_header);
			}
			$req = cURL::newJsonRequest($method, $this->resource_url . $path, $payload)
				->setOption(CURLOPT_FOLLOWLOCATION, true)
				->setHeader($auth_header[0], $auth_header[1])
				->setOption(CURLOPT_USERAGENT, 'Weelis marketing client/1.0');
			$res = $req->send();

			if ($res->statusCode == 200) {
				return json_decode($res->body, true);
			}

			return json_decode($res->body, true);
		}

		return 'Lỗi truy cập';
	}

	public function getToken($code, $redirect_uri = '', $grant_type = 'authorization_code')
	{
		$token_key = $this->token_cache_key . ':' . $code;
		if (Cache::has($token_key)) {
			return Cache::get($token_key);
		}
		// Issue token
		$req = cURL::newRequest('POST', $this->token_url, [
			'client_id'     => $this->client_id,
			'client_secret' => $this->client_secret,
			'code'          => $code,
			'grant_type'    => $grant_type,
			'redirect_uri'  => $redirect_uri
		])
			->setOption(CURLOPT_FOLLOWLOCATION, true)
			->setOption(CURLOPT_USERAGENT, 'Weelis marketing client/1.0')
			->setHeader('Content-Type', $this->content_type);
		$res = $req->send();
		$active_data = json_decode($res->body, true);
		if (isset($active_data['access_token'])) {
			$expired_in = isset($active_data['expires_in']) ? $active_data['expires_in'] / 60 : 60;
			$this->setToken($code, $active_data['access_token'], $expired_in);

			return $active_data['access_token'];
		}

		return null;
	}

	public function setToken($code, $token, $expired_in = 60)
	{
		$token_key = $this->token_cache_key . ':' . $code;
		Cache::put($token_key, $token, $expired_in);
	}
}