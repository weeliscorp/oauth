<?php

namespace Weelis\Oauth\Oauth2\Client;

use Cache;
use cURL;
use File;
use Oauth2;
use Helpers;

/**
 * Oauth2 jwt client
 * User: simonnguyen
 * Date: 08/18/16
 * Time: 11:43 AM
 */
class OauthJwtClient
{
    private $private_key;
    private $public_key;

    public function __construct($config = [])
    {
        $this->private_key = $config['private_key'];
        $this->public_key = $config['public_key'];
    }

    public function genJwtToken($client_id, $key, $audience)
    {
        $public_key = File::get($this->public_key);
        $private_key = File::get($this->private_key);
        Oauth2::getStorage('jwt_bearer')->setClientKey($client_id, $key, $public_key);
        $client_info = Oauth2::getStorage('jwt_bearer')->getClientInfo($client_id);
        return $this->generateJWT($private_key, $client_info['client_id'], $client_info['subject'], $audience, time() + (365 * 24 * 60 * 60));
    }

    /**
     * Generate a JWT
     *
     * @param $privateKey The private key to use to sign the token
     * @param $iss The issuer, usually the client_id
     * @param $sub The subject, usually a user_id
     * @param $aud The audience, usually the URI for the oauth server
     * @param $exp The expiration date. If the current time is greater than the exp, the JWT is invalid
     * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
     * @param $jti The "jwt token identifier", or nonce for this JWT
     *
     * @return string
     */
    public static function generateJWT($privateKey, $iss, $sub, $aud, $exp = null, $nbf = null, $jti = null)
    {
        if (!$exp) {
            $exp = time() + 1000;
        }

        $params = array(
            'iss' => $iss,
            'sub' => $sub,
            'aud' => $aud,
            'exp' => $exp,
            'iat' => time(),
        );

        if ($nbf) {
            $params['nbf'] = $nbf;
        }

        if ($jti) {
            $params['jti'] = $jti;
        }

        $jwtUtil = new \OAuth2\Encryption\Jwt();

        return $jwtUtil->encode($params, $privateKey, 'RS256');
    }
}