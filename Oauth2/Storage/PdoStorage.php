<?php

namespace Weelis\Oauth\Oauth2\Storage;

use Auth;
use OAuth2\Storage\Pdo;

class PdoStorage extends Pdo
{
	/* JWTBearerInterface */
	public function setClientKey($client_id, $client_key, $subject = null)
	{
		if ($this->getClientKey($client_id, $subject)) {
			$stmt = $this->db->prepare(sprintf('UPDATE %s SET subject=:client_key, public_key=:subject where client_id=:client_id', $this->config['jwt_table']));
		} else {
			$stmt = $this->db->prepare(sprintf('INSERT INTO %s (client_id, subject, public_key) VALUES (:client_id, :client_key, :subject)', $this->config['jwt_table']));
		}

		return $stmt->execute(compact('client_id', 'client_key', 'subject'));
	}

	public function unsetClientKey($client_id)
	{
		$stmt = $this->db->prepare(sprintf('DELETE FROM %s WHERE client_id = :client_id', $this->config['jwt_table']));

		$stmt->execute(compact('client_id'));

		return $stmt->rowCount() > 0;
	}

	public function checkUserCredentials($username, $password)
	{
		$status = false;
		foreach ($this->config['username_fields'] as $username_field) {
			$data = [$username_field => $username];
			if(isset($this->config['active_field'])) {
				$data[$this->config['active_field']] = 1;
			}
			if(isset($this->config['social_field'])) {
				$user_builder = app()->make($this->config['user_model'])
				->where($this->config['social_field'], $password);
				foreach ($data as $key => $value) {
					$user_builder = $user_builder->where($key, $value);
				}
				if ($user = $user_builder->first())
				{
					Auth::login($user);
					$status = true;
					break;
				}
			}			
			$data[$this->config['password_field']] = $password;
			if ($user = $this->guard()->attempt($data)) {
				$status = true;
				break;
			}
		}
		

		return $status;
	}

	public function getUser($username)
	{
		$model = app()->make($this->config['user_model'])->where(function($query) use ($username) {
			foreach ($this->config['username_fields'] as $value) {
				$query->orWhere($value, $username);
			}
		});
		if($user = $model->first()) {
			return array_merge(array(
				'user_id' => $user->{$this->config['userid_field']},
			), $user->toArray());
		}
		return false;
	}

	protected function guard()
	{
		return Auth::guard();
	}

}
